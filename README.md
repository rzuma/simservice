# simservice



## Getting started

0. `poetry install`

0. `poetry shell`

0. `poe test`

0. `poe serve`

0. `poe ci`

0. `poe cd`


## Add your files

serve.env
```
MODEL_URI="runs:/$run_id/similarity"
PORT=8000
TRACKING_URI="http://$mlflow:5000"
MLFLOW_S3_ENDPOINT_URL="http://$minio:9999"
AWS_ACCESS_KEY_ID="minioadmin"
AWS_SECRET_ACCESS_KEY="$password"
```

test.env
```

```

ci.env
```
NEXUS_REGISTRY_IMAGE="$nexus:8081/repository/sim/service"
NEXUS_REGISTRY_USER="admin"
NEXUS_REGISTRY_PASSWORD="$password"
NEXUS_REGISTRY="$nexus:8081"
CI_COMMIT_REF_SLUG="feature-example"
```

cd.env
```
SSH_URI="$username@$hostname"
SSH_PRIVATE_KEY="-----BEGIN OPENSSH PRIVATE KEY-----\n$private_key\n-----END OPENSSH PRIVATE KEY-----"
NEXUS_REGISTRY_IMAGE="$nexus:8081/repository/sim/service"
NEXUS_REGISTRY_USER="admin"
NEXUS_REGISTRY_PASSWORD="$password"
NEXUS_REGISTRY="$nexus:8081"
CI_COMMIT_REF_SLUG="feature-example"
```
